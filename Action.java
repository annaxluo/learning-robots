package nnetrobots;

/**
 * defines the Action class for robots that specifies the actions to be
 * taken, and their integer reresentations.
 */
public class Action
{
  public static final int forward = 0;
  public static final int backward = 1;
  public static final int leftForward = 2;
  public static final int rightForward = 3;
  public static final int leftBackward = 4;
  public static final int rightBackward = 5;
  public static final int numActions = 6;

  public static final double moveDistance = 300.0;
  public static final double turnDegrees =  20.0;
}
