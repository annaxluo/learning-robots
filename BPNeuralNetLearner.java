package nnetrobots;

import java.io.*; 
import java.util.*; 

/**
 * defines a neural network that learns to approximate a lookup table 
 */
public class BPNeuralNetLearner {
  public static final double learningRate = 0.1; 
  public static final double discountRate = 0.9; 
  public static final double explorationRate = 0.0; 
  
  // factors for scaling the Q values
  public static final double QMean = 0.56154; 
  public static final double QStd = 2.4082; 
  
  private double[] lastState; 
  private int lastAction; 
  private boolean first = true; 
  private BPNeuralNet net; 
  private Rewards rewards; 
  
  public BPNeuralNetLearner( BPNeuralNet net ){
	this.net = net; 
	rewards = new Rewards(); 
  }
  
  public double getTotalRewards(){
	return rewards.getTotalRewards(); 
  }
	  
  // for terminal rewards only cases
  public void updateRewards( double r ){
	rewards.addRewards( r );  
  }
  
  public void learnOffPolicy( double[] state, int action, double reinforcement ){
	System.out.println( "Instant reward: " + reinforcement ); 
	
	if( first ){
	  first = false; 
	}else{
	  double oldQValue = net.outputFor( lastState, lastAction ); 
	  double newQValue = getMaxQValue( state ); 
	  
	  // scale the target to [-1 1]
	  double reinforcementScaled = ( reinforcement - QMean ) / QStd; 
	  
	  double errorQ = learningRate * ( reinforcementScaled + 
			  discountRate * newQValue - oldQValue ); 
	  
	  double error = net.train( lastState, lastAction, oldQValue + errorQ ); 
	  
	}
	lastState = state; 
	lastAction = action; 
	rewards.addRewards( reinforcement );
  }
  
  public void saveRewards( File file ){
	try{
	  rewards.saveRewards( file );
	}catch( Exception e ){
	  e.printStackTrace();
	}
  }

  // helper functions
  // select action on e-greedy
  public int selectAction( double[] state ){
	Random r = new Random(); 
	int action; 
	
	if( r.nextDouble() < explorationRate ){
	  action = r.nextInt( Action.NumRobotActions ); 
	}else{
	  action = getBestAction( state );
	}
	
	return action; 
  }
  
  // return max Q value for a state
  private double getMaxQValue( double[] state ){
	int bestAction = getBestAction( state ); 
	return net.outputFor( state, bestAction ); 
	
  }
  
  // return action with max Q value for a state
  private int getBestAction( double[] state ){
	double[] allQValues = net.outputFor( state );  
	double bestValue = Double.NEGATIVE_INFINITY; 
	int bestAction = 0; 
	
	for( int i = 0; i < Action.NumRobotActions; i++ ){
	  if( allQValues[i] > bestValue ){
		bestValue = allQValues[i]; 
		bestAction = i;
	  }
	}
	return bestAction; 
  }
}
