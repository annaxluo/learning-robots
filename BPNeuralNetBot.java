package nnetrobts;

import java.awt.*;
import java.awt.geom.*;
import java.io.IOException;
import java.io.PrintStream;

import robocode.*;

/**
 * defines a robot that uses a neural net to approximate the lookup table
 * for reinforcement learning
 */
public class BPNeuralNetBot extends AdvancedRobot
{
  public static final double PI = Math.PI;
  
  // neural net params
  private static final int NUM_INPUTS = 5; 
  private static final int NUM_HIDDEN = 12; 
  private static final int NUM_OUTPUTS = 6; 
  private static final double NN_LEARN_RATE = 0.01; 
  private static final double[] TEST_STATE_1 = new double[]{ -1.0, 2.0, -1.0, 0.0, 0.0 }; 
  private static final double[] TEST_STATE_2 = new double[]{ -1.0, 8.0, 1.0, 0.0, 1.0 }; 
  private static final double[] TEST_STATE_3 = new double[]{ 1.0, 18.0, 1.0, 1.0, 0.0 };
  private static final int TEST_ACTION_1 = 2; 
  private static final int TEST_ACTION_2 = 4; 
  private static final int TEST_ACTION_3 = 0; 
  private static final boolean QUANTIZE_STATE = true; 
  
  // robot params
  private Target target;
  private BPNeuralNet net; 
  private BPNeuralNetLearner learner; 
  private double reinforcement = 0.0;
  private double firePower;
  private int direction = 1;
  private double isHitWall = 0;
  private double isHitByBullet = 0;

  public void run()
  {
	net = new BPNeuralNet( NUM_INPUTS, NUM_HIDDEN, NUM_OUTPUTS, NN_LEARN_RATE );
    loadData();
    learner = new BPNeuralNetLearner( net ); 
    target = new Target();
    target.distance = 100000;

    setColors(Color.green, Color.white, Color.green);
    setAdjustGunForRobotTurn(true);
    setAdjustRadarForGunTurn(true);
    turnRadarRightRadians(2 * PI);
    while (true)
    {
      move();
      firePower = 400/target.distance;
      if (firePower > 3)
        firePower = 3;
      sense();
      aim();
      if (getGunHeat() == 0) {
        setFire(firePower);
      }
      execute();
    }
  }

  private void move()
  {
	// change state to double[]
    double state[] = getState();
    int action = learner.selectAction( state );
    
    out.println("Action selected: " + action);
    
    // do reinforcement learning
    learner.learnOffPolicy( state, action, reinforcement );
    
    reinforcement = 0.0;
    isHitWall = 0.0;
    isHitByBullet = 0.0;

    switch (action)
    {
      case Action.forward:
        setAhead(Action.moveDistance);
        break;
      case Action.backward:
        setBack(Action.moveDistance);
        break;
      case Action.leftForward:
        setAhead(Action.moveDistance);
        setTurnLeft(Action.turnDegrees);
        break;
      case Action.rightForward:
        setAhead(Action.moveDistance);
        setTurnRight(Action.turnDegrees);
        break;     
      case Action.leftBackward:
        setAhead(Action.moveDistance);
        setTurnRight(Action.turnDegrees);
        break;
      case Action.rightBackward:
        setAhead(target.bearing);
        setTurnLeft(Action.turnDegrees);
        break;
    }
  }

  // change to return double[]
  private double[] getState()
  {
	double[] state = new double[NUM_INPUTS]; 
	
    state[0] = getStateHeading(); 
    state[1] = getStateTargetDistance(); 
    state[2] = getStateTargetBearing(); 
    state[3] = isHitWall; 
    state[4] = isHitByBullet; 
    
    out.println("Stste(" + state[0] + ", " + state[1] + ", " + state[2] + ", " + state[3] + ", " + state[4] + ")");
    
    return state;
  }
  
  private double getStateHeading(){
	double h = getHeading();
	double newHeading = 0.0; 
	if( h >= 0 && h <= 180 ){
	  newHeading = h / 90;
	}else{
	  h -= 360; 
	  newHeading = h / 90; 
	}
	
	if( QUANTIZE_STATE )
	  newHeading = (int)newHeading; 
	
	return newHeading; 
  }
  
  private double getStateTargetDistance(){
	double distance = target.distance / 30.0; 
	if( distance > State.NumTargetDistance - 1 )
	  distance = (double)State.NumTargetDistance - 1; 
	
	if( QUANTIZE_STATE )
	  distance = (int)distance; 
	
	return distance; 
  }
  
  private double getStateTargetBearing(){
	double bearing = target.bearing; 
	double newBearing = bearing / (Math.PI / 2); 
	
	if( QUANTIZE_STATE )
	  newBearing = (int)newBearing; 
	
	return newBearing; 
  }
  
  private void sense()
  {
    double radarOffset;
    if (getTime() - target.ctime > 4) { 
      radarOffset = 4*PI;				
    } else {

      radarOffset = getRadarHeadingRadians() - (Math.PI/2 - Math.atan2(target.y - getY(),target.x - getX()));

      radarOffset = NormaliseBearing(radarOffset);
      if (radarOffset < 0)
        radarOffset -= PI/10;
      else
        radarOffset += PI/10;
    }
    //turn the radar
    setTurnRadarLeftRadians(radarOffset);
  }

  private void aim()
  {
    long time;
    long nextTime;
    Point2D.Double p;
    p = new Point2D.Double(target.x, target.y);
    for (int i = 0; i < 20; i++)
    {
      nextTime = (int)Math.round((getrange(getX(),getY(),p.x,p.y)/(20-(3*firePower))));
      time = getTime() + nextTime - 10;
      p = target.guessPosition(time);
    }
    double gunOffset = getGunHeadingRadians() - (Math.PI/2 - Math.atan2(p.y - getY(),p.x -  getX()));
    setTurnGunLeftRadians(NormaliseBearing(gunOffset));
  }
  
  //bearing is within the -pi to pi range
  double NormaliseBearing(double ang) {
    if (ang > PI)
      ang -= 2*PI;
    if (ang < -PI)
      ang += 2*PI;
    return ang;
  }

  //heading within the 0 to 2pi range
  double NormaliseHeading(double ang) {
    if (ang > 2*PI)
      ang -= 2*PI;
    if (ang < 0)
      ang += 2*PI;
    return ang;
  }

  //returns the distance between two x,y coordinates
  public double getrange( double x1,double y1, double x2,double y2 )
  {
    double xo = x2-x1;
    double yo = y2-y1;
    double h = Math.sqrt( xo*xo + yo*yo );
    return h;
  }

  //gets the absolute bearing between to x,y coordinates
  public double absbearing( double x1,double y1, double x2,double y2 )
  {
    double xo = x2-x1;
    double yo = y2-y1;
    double h = getrange( x1,y1, x2,y2 );
    if( xo > 0 && yo > 0 )
    {
      return Math.asin( xo / h );
    }
    if( xo > 0 && yo < 0 )
    {
      return Math.PI - Math.asin( xo / h );
    }
    if( xo < 0 && yo < 0 )
    {
      return Math.PI + Math.asin( -xo / h );
    }
    if( xo < 0 && yo > 0 )
    {
      return 2.0*Math.PI - Math.asin( -xo / h );
    }
    return 0;
  }

  public void onBulletHit(BulletHitEvent e)
  {
    if (target.name == e.getName())
    {
      //double power = e.getBullet().getPower();
      //double change = 4 * power + 2 * (power - 1);
      double change = e.getBullet().getPower() * 9;
      out.println("Bullet Hit: " + change);
      reinforcement += change;
    }
  }


  public void onBulletMissed(BulletMissedEvent e)
  {
    double change = -e.getBullet().getPower();
    out.println("Bullet Missed: " + change);
    reinforcement += change;
  }

  public void onHitByBullet(HitByBulletEvent e)
  {
    if (target.name == e.getName())
    {
      double power = e.getBullet().getPower();
      //double change = -(4 * power + 2 * (power - 1));
      double change = -4 * power; 
      if( power > 1 )
    	change -= 2 * (power - 1); 
      out.println("Hit By Bullet: " + change);
      reinforcement += change;
    }
    isHitByBullet = 1;
  }

  public void onHitRobot(HitRobotEvent e)
  {
    if (target.name == e.getName())
    {
      double change = +2.0;
      out.println("Hit Robot: " + change);
      reinforcement += 8.0;
    }
  }

  public void onHitWall(HitWallEvent e)
  {
    
    double change = -(Math.abs(getVelocity()) * 0.5 - 1);
    out.println("Hit Wall: " + change);
    reinforcement -= 10.0;
    isHitWall = 1;
  }

  public void onScannedRobot(ScannedRobotEvent e)
  {
    if ((e.getDistance() < target.distance)||(target.name == e.getName()))
    {
      double absbearing_rad = (getHeadingRadians()+e.getBearingRadians())%(2*PI);
      target.name = e.getName();
      double h = NormaliseBearing(e.getHeadingRadians() - target.head);
      h = h/(getTime() - target.ctime);
      target.changehead = h;
      target.x = getX()+Math.sin(absbearing_rad)*e.getDistance(); 
      target.y = getY()+Math.cos(absbearing_rad)*e.getDistance(); 
      target.bearing = e.getBearingRadians();
      //target.bearing = e.getBearing(); 
      target.head = e.getHeadingRadians();
      target.ctime = getTime();				
      target.speed = e.getVelocity();
      target.distance = e.getDistance();
      target.energy = e.getEnergy();
      
      if( e.getDistance() < 30 )
    	reinforcement += 8; 
      else
    	reinforcement -=5; 
    }
  }

  public void onRobotDeath(RobotDeathEvent e)
  {

    if (e.getName() == target.name)
      target.distance = 10000;
  }

  public void onWin(WinEvent event)
  {
    reinforcement += 15; 
	move(); 
    saveData();
    saveRewards();
    saveRoundResults( 1 ); 
  }

  public void onDeath(DeathEvent event)
  {
    reinforcement -= 15; 
	move(); 
    saveData();
    saveRewards(); 
    saveRoundResults( 0 ); 
  }
  
  
  // test the three specific state action pairs
  private void testStateActionPairs(){
    double newTestQ1 = net.outputFor( TEST_STATE_1, TEST_ACTION_1 ); 
	double newTestQ2 = net.outputFor( TEST_STATE_2, TEST_ACTION_2 );
	double newTestQ3 = net.outputFor( TEST_STATE_3, TEST_ACTION_3 ); 
	 
	// write to file
	PrintStream w = null;
	try{
	  w = new PrintStream( 
			  new RobocodeFileOutputStream( getDataFile( "test.txt" ).toString(), 
					  true ) );
	  w.print( newTestQ1 + "\t" + newTestQ2 + "\t" + newTestQ3 + "\n" );
	  
	  if( w.checkError() )
	    System.out.println( "Cannot write results" ); 
	      
	  w.close();
	}catch( IOException e ){
	  e.printStackTrace();
	}finally{
	  try{
	    if( w != null )
	      w.close();
	    }catch( Exception e ){
	      e.printStackTrace();
	    }
	}
  }
  
  // terminal rewards
  public void saveRewards(){
    try{
      learner.saveRewards( getDataFile( "rewards.txt" ) );
    }catch( Exception e ){
      e.printStackTrace();
    }
  }
  
  // save result (winning rate)
  public void saveRoundResults( int res ){
	PrintStream w = null; 
    try{
      w = new PrintStream( 
    		  new RobocodeFileOutputStream( getDataFile( "results.txt" ).toString(), 
    				  true ) );  // append
      w.println( res ); 
      
      if( w.checkError() )
    	System.out.println( "Cannot write results" ); 
      
      w.close();
    }catch( IOException e ){
      e.printStackTrace();
    }finally{
      try{
    	if( w != null )
    	  w.close();
      }catch( Exception e ){
    	e.printStackTrace();
      }
    }
  }

  public void loadData()
  {
    try
    {
      net.load( getDataFile( "movement.dat" ) );
    }
    catch (Exception e)
    {
    }
  }

  public void saveData()
  {
    try
    {
      net.save(getDataFile( "movement.dat" ));
    }
    catch (Exception e)
    {
      out.println("Exception trying to write: " + e);
    }
  }
}
