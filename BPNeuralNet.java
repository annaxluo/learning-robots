package nnetrobots;

import java.util.Random;
import robocode.RobocodeFileOutputStream;
import java.io.*; 

/**
 * defines a backpropagtion neural network  
 */
public class BPNeuralNet {
  private int numInputs; 
  private int numHidden; 
  private int numOutputs; 
  
  private double learningRate; 
  
  private double[][] wih; 
  private double[][] who; 
  
  private double[] inputs;
  private double[] hidden; 
  private double[] target; 
  private double[] actual; 
  
  private double[] erro; 
  private double[] errh; 

  // constructor
  public BPNeuralNet( int nInp, int nHid, int nOut, double lr ){
	numInputs = nInp; 
	numHidden = nHid; 
	numOutputs = nOut; 
	
	learningRate = lr; 
	
	// Input to Hidden Weights (with Biases).
	wih = new double[numInputs + 1][numHidden]; 
	// Hidden to Output Weights (with Biases).
	who = new double[numHidden + 1][numOutputs]; 
	
	// Activations
	inputs = new double[numInputs]; 
	hidden = new double[numHidden]; 
	target = new double[numOutputs]; 
	actual = new double[numOutputs]; 
	
	// errors
	erro = new double[numOutputs]; 
	errh = new double[numHidden]; 
	
	assignRandomWeights(); 
  }
  
  // return output for the state-action pair
  public double outputFor( double[] theInputs, int outIndex ){
    setInputs( theInputs ); 
    
    feedForward(); 
    
    return actual[outIndex]; 
  }
  
  // override: return all outputs
  public double[] outputFor( double[] theInputs ){
	  setInputs( theInputs ); 
	  feedForward(); 
	  return actual; 
  }
  
  // train: feedforward + backprop
  public double train( double[] theInputs, int outIndex, double targetVal ){
	setInputs( theInputs ); 
	
	feedForward(); 
	
	for( int k = 0; k < numOutputs; k++ ){
	  if( k == outIndex ){
		target[k] = targetVal; 
	  }else{
		target[k] = actual[k]; 
	  }
	}

	// update weights
	backPropagate(); 
	
	// get the training statistics
	feedForward(); 

	double error = actual[outIndex] - targetVal; 
	  
	return error * error; // squared error
  }
  
  // save the weights
  public void save( File file ){
	PrintStream w = null; 
	try{
      w = new PrintStream( new FileOutputStream( file ) ); 
      //w = new PrintStream( new RobocodeFileOutputStream( file ) ); 
      
      // print 
      double[] vec = weightsBiasVector(); 
	  for( int i = 0; i < vec.length; i++ )
		w.println( new Double( vec[i] ) ); 
		  
		if( w.checkError() )
		  System.out.println( "Cannot save data." ); 
		
		w.close();
    }catch( IOException e ){
	  System.out.println( "IOException trying to write" + e ); 
	}finally{
	  try{
		if( w != null )
		  w.close(); 
	  }catch( Exception e ){
		e.printStackTrace();
	  }
	}
  }
  
  public void load( File file ){
	BufferedReader r = null; 
	try{
	  r = new BufferedReader( new FileReader( file ) ); 
	  double[] vec = new double[(numInputs+1)*numHidden+(numHidden+1)*numOutputs]; 
	  for( int i = 0; i < vec.length; i++ )
		vec[i] = Double.parseDouble( r.readLine() ); 
	
	  int count = 0; 
	  for( int i = 0; i <= numInputs; i++ ){
		for( int h = 0; h < numHidden; h++ ){
		  wih[i][h] = vec[count++]; 
		}
	  }
		
	  for( int h = 0; h <= numHidden; h++ ){
		for( int o = 0; o < numOutputs; o++ ){
	      who[h][o] = vec[count++]; 
		}
	  }
	  
	}catch( IOException e ){
	  System.out.println( "Random Init" ); 
	  assignRandomWeights(); 
	}catch( NumberFormatException e ){
      System.out.println( "Random Init" );
	  assignRandomWeights();
	}finally{
	  try{
	   	if( r != null )
	   	  r.close(); 
	  }catch( IOException e ){
	   	e.printStackTrace();
	}
	}
  }
  
  // get weights in a vector
  private double[] weightsBiasVector(){
	double[] vec = new double[(numInputs+1)*numHidden + (numHidden+1)*numOutputs]; 
	int count = 0; 
	for( int i = 0; i <= numInputs; i++ ){
	  for( int h = 0; h < numHidden; h++ )
		vec[count++] = wih[i][h]; 
	}
	
	for( int h = 0; h <= numHidden; h++ ){
	  for( int o = 0; o < numOutputs; o++ ){
		vec[count++] = who[h][o]; 
	  }
	}
	
	return vec; 
  }
  
  // set the input neuron values
  private void setInputs( double[] val ){
	for( int i = 0; i < numInputs; i++ )
	  inputs[i] = val[i]; 
  }
  
  // outputFor: feedForward
  private void feedForward()
  {
    double sum = 0.0;

    // Calculate input to hidden layer.
    for(int hid = 0; hid < numHidden; hid++)
    {
      sum = 0.0;
      for(int inp = 0; inp < numInputs; inp++)
      {
        sum += inputs[inp] * wih[inp][hid];
      } // inp

      sum += wih[numInputs][hid]; // Add in bias.
      hidden[hid] = sigmoid(sum);
    } // hid

    // Calculate the hidden to output layer.
    for(int out = 0; out < numOutputs; out++)
    {
      sum = 0.0;
      for(int hid = 0; hid < numHidden; hid++)
      {
        sum += hidden[hid] * who[hid][out];
      } // hid

      sum += who[numHidden][out]; // Add in bias.
      actual[out] = sigmoid(sum);
      
    } // out    
    
    return;
  }
  
  private void backPropagate()
  {
    // Calculate the output layer error (step 3 for output cell).
    for(int out = 0; out < numOutputs; out++)
    {
      erro[out] = (target[out] - actual[out]) * sigmoidDerivative(actual[out]);
    }

    // Calculate the hidden layer error (step 3 for hidden cell).
    for(int hid = 0; hid < numHidden; hid++)
    {
      errh[hid] = 0.0;
      for(int out = 0; out < numOutputs; out++)
      {
        errh[hid] += erro[out] * who[hid][out];
      }
        errh[hid] *= sigmoidDerivative(hidden[hid]);
    }

    // Update the weights for the output layer (step 4).
    for(int out = 0; out < numOutputs; out++)
    {
      for(int hid = 0; hid < numHidden; hid++)
      {
        who[hid][out] += (learningRate * erro[out] * hidden[hid]);
      } // hid
      who[numHidden][out] += (learningRate * erro[out]); // Update the bias.
    } // out

    // Update the weights for the hidden layer (step 4).
    for(int hid = 0; hid < numHidden; hid++)
    {
      for(int inp = 0; inp < numInputs; inp++)
      {
        wih[inp][hid] += (learningRate * errh[hid] * inputs[inp]);
      } // inp
      wih[numInputs][hid] += (learningRate * errh[hid]); // Update the bias.
    } // hid
    
    return;
  }
  
  private void assignRandomWeights()
  {
     for(int inp = 0; inp <= numInputs; inp++) // Do not subtract 1 here.
     {
        for(int hid = 0; hid < numHidden; hid++)
        {
            // Assign a random weight value between -1 and 1
            wih[inp][hid] = new Random().nextDouble() * 2 - 1;
        } // hid
     } // inp

     for(int hid = 0; hid <= numHidden; hid++) // Do not subtract 1 here.
     {
        for(int out = 0; out < numOutputs; out++)
        {
            // Assign a random weight value between -1 and 1
            who[hid][out] = new Random().nextDouble() * 2 - 1;
        } // out
     } // hid
      return;
  }

  private double sigmoid( final double val )
  {
      //return (1.0 / (1.0 + Math.exp(-val)));
  	return 2 / (1 + Math.exp(-val)) - 1; 
  }

  private double sigmoidDerivative( final double val )
  {
      //return (val * (1.0 - val));
  	return (0.5)*(1 + val)*(1 - val); 
  }
  
  
}
