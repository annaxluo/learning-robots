package nnetrobots;

/**
 * defines the integer representations of states
 */
public class State
{
  public static final int numTargetDistance = 20;
  public static final int numHitWall = 2;
  public static final int numHeading = 4;
  public static final int numTargetBearing = 4;
  public static final int numHitByBullet = 2;
  public static final int Mapping[][][][][];

  public static final int numStates;

  static
  {
    Mapping = new int[numHeading][numTargetDistance][numTargetBearing][numHitWall][numHitByBullet];
    int count = 0;
    for (int a = 0; a < numHeading; a++)
      for (int b = 0; b < numTargetDistance; b++)
        for (int c = 0; c < numTargetBearing; c++)
          for (int d = 0; d < numHitWall; d++)
            for (int e = 0; e < numHitByBullet; e++)
          Mapping[a][b][c][d][e] = count++;

    numStates = count;
  }

  
  public static int getHeading(double heading)
  {
    double angle = 360 / numHeading;
    double newHeading = heading + angle / 2;
    if (newHeading >= 360.0) // original: >
      newHeading -= 360.0;
    return (int)(newHeading / angle);
  }

  public static int getTargetDistance(double value)
  {
    int distance = (int)(value / 30.0);
    if (distance > numTargetDistance - 1)
      distance = numTargetDistance - 1;
    return distance;
  }
  
  public static int getTargetBearing(double bearing)
  {
    double PIx2 = Math.PI * 2;
    if (bearing < 0)
      bearing = PIx2 + bearing;
    double angle = PIx2 / numTargetBearing;
    double newBearing = bearing + angle / 2;
    if (newBearing >= PIx2) // original: >
      newBearing -= PIx2;
    return (int)(newBearing / angle);

  }
 
}
