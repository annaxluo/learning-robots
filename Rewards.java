/**
 * defines the Rewards class for step rewards and total round rewards
 */
package nnetrobots; 

import java.io.*;
import java.util.ArrayList;
import robocode.*; 

public class Rewards {

  private double totalRewards; 
  private ArrayList<Double> rewards;
  
  public Rewards(){
	totalRewards = 0.0; 
	rewards = new ArrayList<Double>(); 
  }
  
  public void addRewards( double r ){
	totalRewards += r; 
	rewards.add( r ); 
  }
  
  public double getTotalRewards(){
	return totalRewards; 
  }
  
  public ArrayList<Double> getStepRewards(){
	return rewards; 
  }
  
  public void saveRewards( File file ){
    PrintStream w = null; 
    try{
  	  w = new PrintStream( new RobocodeFileOutputStream( file.toString(), true ) ); 
  	  /*for( int i = 0; i < rewards.size(); i++ ){
  		w.println( new Double( rewards.get( i ) ) ); 
  	  }*/
  	  w.println( totalRewards ); 
  	  
  	  if( w.checkError() )
  		System.out.println( "Cannot write rewards." );
  	  
  	  w.close();
  	}catch( IOException e ){
  	  e.printStackTrace();
  	}finally{
  	  try{
  		if( w != null )
  		  w.close();
  	  }catch( Exception e ){
  		e.printStackTrace();
  	  }
  	}
  }
  
}
